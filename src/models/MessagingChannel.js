import { HttpBadRequestError, HttpConflictError } from '@themost/common';
import { DataObject, DataObjectState, EdmMapping, EdmType} from '@themost/data';

@EdmMapping.entityType('MessagingChannel')
class MessagingChannel extends DataObject {

    /** @type {number=} */
    id;
    /** @type {string=} */
    name;
    /** @type {string=} */
    alternateName;
    /** @type {string=} */
    description;
    /** @type {Date=} */
    dateCreated;
    /** @type {Date=} */
    dateModified;
    /** @type {*=} */
    createdBy;
    /** @type {*=} */
    modifiedBy;
    /** @type {string=} */
    headline;
    /** @type {number|{id:number,name:string,alternateName:string}} */
    maintainer;
    /** @type {Date=} */
    expiresAt;
    /** @type {string=} */
    thumbnailUrl;
    /** @type {boolean=} */
    isDirect;
    /** @type {boolean=} */
    isDisabled;
    /** @type {Array<string>=} */
    keywords;
    /** @type {number|{id:number,name:string,alternateName:string}} */
    type;


    constructor() {
        super();
    }

    /**
     * @returns {import("@themost/data").DataQueryable}
     */
    @EdmMapping.param('$top', EdmType.EdmInt32, false, false)
    @EdmMapping.param('$skip', EdmType.EdmInt32, false, false)
    @EdmMapping.func('Messages', EdmType.CollectionOf('InstantMessage'))
    get($skip, $top) {
        return this.context.model('InstantMessage').where('recipient').equal(this.id).skip(parseInt($skip)).take(parseInt($top)).prepare();
    }


    /**
     * @returns {import("@themost/data").DataQueryable}
     */
    @EdmMapping.param('message', 'InstantMessage', false, true)
    @EdmMapping.action('Messages', 'InstantMessage')
    async post(message) {
        await this.context.model('InstantMessage').insert(Object.assign(message, {
            recipient: this.id
        }));
        delete message.$state;
        return message;
    }

        /**
     * @returns {import("@themost/data").DataQueryable}
     */
        @EdmMapping.param('message', 'InstantMessage', false, true)
        @EdmMapping.action('removeMessage', 'InstantMessage')
        async removeMessage(message) {
            const item = await this.context.model('InstantMessage').find(message).and('recipient').equal(this.getId()).getItem();
            if(!item) throw new HttpBadRequestError("Instant message can not be found.");
            const children = await this.context.model('InstantMessage').where('parentInstantMessage').equal(item.id).getItems();
            await this.context.model('InstantMessage').remove(children);
            await this.context.model('InstantMessage').remove(item);
            return {item, children};
        }

    /**
     * @returns {import("@themost/data").DataQueryable}
     */
        @EdmMapping.param('message', 'InstantMessage', false, true)
        @EdmMapping.action('toggleMessageReply', 'InstantMessage')
        async toggleMessageReply(message) {
            const item = await this.context.model('InstantMessage').find(message).and('recipient').equal(this.getId()).getItem();
            if(!item) throw new HttpBadRequestError("Instant message can not be found.");
            const itemNew = {...item, hasReply: !item.hasReply}
            return (await this.context.model("InstantMessage").save(itemNew));
        }

        /**
     * @returns {import("@themost/data").DataQueryable}
     */
        @EdmMapping.param('reaction', 'InstantMessageReaction', false, true)
        @EdmMapping.action('toggleReaction', 'InstantMessageReaction')
        async toggleReaction(reaction) {
            if(!reaction.instantMessage) throw new HttpBadRequestError("Target message can not be empty.");
            const isExist = await this.context.model("InstantMessage").find(reaction.instantMessage).and('recipient').equal(this.getId()).count()
            if(!isExist) throw new HttpConflictError("Target message can not be found.")
            // get im account
            const user = await this.context.model('User').where('name').equal(this.context.user.name).select('imAccount/id').value();
            const instantMessage = reaction.instantMessage.id;
            const item = await this.context.model('InstantMessageReaction').find({
                instantMessage,
                user
            }).getItem();
            return item ? (await this.context.model("InstantMessageReaction").remove(item)) : (await this.context.model("InstantMessageReaction").save(reaction))
        }


    /**
     * @returns {import("@themost/data").DataObjectJunction}
     */
    @EdmMapping.func('Members', EdmType.CollectionOf('InstantMessageAccount'))
    getMembers() {
        return this.property('members');
    }

    /**
     * @returns {import("@themost/data").DataQueryable}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('InstantMessageAccount'), false, true)
    @EdmMapping.action('Members', EdmType.CollectionOf('InstantMessageAccount'))
    async setMembers(items) {
        const members = this.getMembers();
        await new Promise((resolve, reject) => {
            void this.context.db.executeInTransaction((cb) => {
                (async () => {
                    await members.insert(items.filter((item) => {
                        return !(item.$state && item.$state === DataObjectState.Delete);
                    }));
                    await members.remove(items.filter((item) => {
                        return (item.$state && item.$state === DataObjectState.Delete);
                    }));
                })().then(() => {
                    return cb();
                }).catch((err) => {
                    return cb(err);
                });
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve();
            });
        });
        return members;
    }

}

export {
    MessagingChannel
}